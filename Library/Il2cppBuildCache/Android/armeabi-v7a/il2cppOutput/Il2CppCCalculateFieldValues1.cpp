﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// LTDescr[]
struct LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// LTDescr
struct LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F;
// System.String
struct String_t;
// DentedPixel.LTExamples.TestingUnitTests
struct TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25
struct U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_2;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<cubeCount>5__2
	int32_t ___U3CcubeCountU3E5__2_3;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<tweensA>5__3
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___U3CtweensAU3E5__3_4;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<aGOs>5__4
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___U3CaGOsU3E5__4_5;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<tweensB>5__5
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___U3CtweensBU3E5__5_6;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<bGOs>5__6
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___U3CbGOsU3E5__6_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcubeCountU3E5__2_3() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CcubeCountU3E5__2_3)); }
	inline int32_t get_U3CcubeCountU3E5__2_3() const { return ___U3CcubeCountU3E5__2_3; }
	inline int32_t* get_address_of_U3CcubeCountU3E5__2_3() { return &___U3CcubeCountU3E5__2_3; }
	inline void set_U3CcubeCountU3E5__2_3(int32_t value)
	{
		___U3CcubeCountU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CtweensAU3E5__3_4() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CtweensAU3E5__3_4)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_U3CtweensAU3E5__3_4() const { return ___U3CtweensAU3E5__3_4; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_U3CtweensAU3E5__3_4() { return &___U3CtweensAU3E5__3_4; }
	inline void set_U3CtweensAU3E5__3_4(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___U3CtweensAU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtweensAU3E5__3_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CaGOsU3E5__4_5() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CaGOsU3E5__4_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_U3CaGOsU3E5__4_5() const { return ___U3CaGOsU3E5__4_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_U3CaGOsU3E5__4_5() { return &___U3CaGOsU3E5__4_5; }
	inline void set_U3CaGOsU3E5__4_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___U3CaGOsU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CaGOsU3E5__4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtweensBU3E5__5_6() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CtweensBU3E5__5_6)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_U3CtweensBU3E5__5_6() const { return ___U3CtweensBU3E5__5_6; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_U3CtweensBU3E5__5_6() { return &___U3CtweensBU3E5__5_6; }
	inline void set_U3CtweensBU3E5__5_6(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___U3CtweensBU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtweensBU3E5__5_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbGOsU3E5__6_7() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CbGOsU3E5__6_7)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_U3CbGOsU3E5__6_7() const { return ___U3CbGOsU3E5__6_7; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_U3CbGOsU3E5__6_7() { return &___U3CbGOsU3E5__6_7; }
	inline void set_U3CbGOsU3E5__6_7(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___U3CbGOsU3E5__6_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbGOsU3E5__6_7), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26
struct U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24
struct U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_2;
	// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>8__1
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F * ___U3CU3E8__1_3;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<descriptionMatchCount>5__2
	int32_t ___U3CdescriptionMatchCountU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E8__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdescriptionMatchCountU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CdescriptionMatchCountU3E5__2_4)); }
	inline int32_t get_U3CdescriptionMatchCountU3E5__2_4() const { return ___U3CdescriptionMatchCountU3E5__2_4; }
	inline int32_t* get_address_of_U3CdescriptionMatchCountU3E5__2_4() { return &___U3CdescriptionMatchCountU3E5__2_4; }
	inline void set_U3CdescriptionMatchCountU3E5__2_4(int32_t value)
	{
		___U3CdescriptionMatchCountU3E5__2_4 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F  : public RuntimeObject
{
public:
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_0;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::pauseCount
	int32_t ___pauseCount_1;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeRound
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeRound_2;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onStartPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___onStartPos_3;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onStartPosSpline
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___onStartPosSpline_4;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeSpline
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeSpline_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeSeq
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeSeq_6;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeBounds
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeBounds_7;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::didPassBounds
	bool ___didPassBounds_8;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::failPoint
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___failPoint_9;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setOnStartNum
	int32_t ___setOnStartNum_10;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setPosOnUpdate
	bool ___setPosOnUpdate_11;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setPosNum
	int32_t ___setPosNum_12;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::hasGroupTweensCheckStarted
	bool ___hasGroupTweensCheckStarted_13;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::previousXlt4
	float ___previousXlt4_14;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onUpdateWasCalled
	bool ___onUpdateWasCalled_15;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::start
	float ___start_16;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::expectedTime
	float ___expectedTime_17;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::didGetCorrectOnUpdate
	bool ___didGetCorrectOnUpdate_18;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__13
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__13_19;
	// System.Action`1<UnityEngine.Vector3> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__14
	Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * ___U3CU3E9__14_20;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__16
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__16_21;
	// System.Action`1<System.Object> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__15
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___U3CU3E9__15_22;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E4__this_0)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_pauseCount_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___pauseCount_1)); }
	inline int32_t get_pauseCount_1() const { return ___pauseCount_1; }
	inline int32_t* get_address_of_pauseCount_1() { return &___pauseCount_1; }
	inline void set_pauseCount_1(int32_t value)
	{
		___pauseCount_1 = value;
	}

	inline static int32_t get_offset_of_cubeRound_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeRound_2)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeRound_2() const { return ___cubeRound_2; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeRound_2() { return &___cubeRound_2; }
	inline void set_cubeRound_2(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeRound_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRound_2), (void*)value);
	}

	inline static int32_t get_offset_of_onStartPos_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___onStartPos_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_onStartPos_3() const { return ___onStartPos_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_onStartPos_3() { return &___onStartPos_3; }
	inline void set_onStartPos_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___onStartPos_3 = value;
	}

	inline static int32_t get_offset_of_onStartPosSpline_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___onStartPosSpline_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_onStartPosSpline_4() const { return ___onStartPosSpline_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_onStartPosSpline_4() { return &___onStartPosSpline_4; }
	inline void set_onStartPosSpline_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___onStartPosSpline_4 = value;
	}

	inline static int32_t get_offset_of_cubeSpline_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeSpline_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeSpline_5() const { return ___cubeSpline_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeSpline_5() { return &___cubeSpline_5; }
	inline void set_cubeSpline_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeSpline_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSpline_5), (void*)value);
	}

	inline static int32_t get_offset_of_cubeSeq_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeSeq_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeSeq_6() const { return ___cubeSeq_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeSeq_6() { return &___cubeSeq_6; }
	inline void set_cubeSeq_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeSeq_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSeq_6), (void*)value);
	}

	inline static int32_t get_offset_of_cubeBounds_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeBounds_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeBounds_7() const { return ___cubeBounds_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeBounds_7() { return &___cubeBounds_7; }
	inline void set_cubeBounds_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeBounds_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeBounds_7), (void*)value);
	}

	inline static int32_t get_offset_of_didPassBounds_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___didPassBounds_8)); }
	inline bool get_didPassBounds_8() const { return ___didPassBounds_8; }
	inline bool* get_address_of_didPassBounds_8() { return &___didPassBounds_8; }
	inline void set_didPassBounds_8(bool value)
	{
		___didPassBounds_8 = value;
	}

	inline static int32_t get_offset_of_failPoint_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___failPoint_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_failPoint_9() const { return ___failPoint_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_failPoint_9() { return &___failPoint_9; }
	inline void set_failPoint_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___failPoint_9 = value;
	}

	inline static int32_t get_offset_of_setOnStartNum_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___setOnStartNum_10)); }
	inline int32_t get_setOnStartNum_10() const { return ___setOnStartNum_10; }
	inline int32_t* get_address_of_setOnStartNum_10() { return &___setOnStartNum_10; }
	inline void set_setOnStartNum_10(int32_t value)
	{
		___setOnStartNum_10 = value;
	}

	inline static int32_t get_offset_of_setPosOnUpdate_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___setPosOnUpdate_11)); }
	inline bool get_setPosOnUpdate_11() const { return ___setPosOnUpdate_11; }
	inline bool* get_address_of_setPosOnUpdate_11() { return &___setPosOnUpdate_11; }
	inline void set_setPosOnUpdate_11(bool value)
	{
		___setPosOnUpdate_11 = value;
	}

	inline static int32_t get_offset_of_setPosNum_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___setPosNum_12)); }
	inline int32_t get_setPosNum_12() const { return ___setPosNum_12; }
	inline int32_t* get_address_of_setPosNum_12() { return &___setPosNum_12; }
	inline void set_setPosNum_12(int32_t value)
	{
		___setPosNum_12 = value;
	}

	inline static int32_t get_offset_of_hasGroupTweensCheckStarted_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___hasGroupTweensCheckStarted_13)); }
	inline bool get_hasGroupTweensCheckStarted_13() const { return ___hasGroupTweensCheckStarted_13; }
	inline bool* get_address_of_hasGroupTweensCheckStarted_13() { return &___hasGroupTweensCheckStarted_13; }
	inline void set_hasGroupTweensCheckStarted_13(bool value)
	{
		___hasGroupTweensCheckStarted_13 = value;
	}

	inline static int32_t get_offset_of_previousXlt4_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___previousXlt4_14)); }
	inline float get_previousXlt4_14() const { return ___previousXlt4_14; }
	inline float* get_address_of_previousXlt4_14() { return &___previousXlt4_14; }
	inline void set_previousXlt4_14(float value)
	{
		___previousXlt4_14 = value;
	}

	inline static int32_t get_offset_of_onUpdateWasCalled_15() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___onUpdateWasCalled_15)); }
	inline bool get_onUpdateWasCalled_15() const { return ___onUpdateWasCalled_15; }
	inline bool* get_address_of_onUpdateWasCalled_15() { return &___onUpdateWasCalled_15; }
	inline void set_onUpdateWasCalled_15(bool value)
	{
		___onUpdateWasCalled_15 = value;
	}

	inline static int32_t get_offset_of_start_16() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___start_16)); }
	inline float get_start_16() const { return ___start_16; }
	inline float* get_address_of_start_16() { return &___start_16; }
	inline void set_start_16(float value)
	{
		___start_16 = value;
	}

	inline static int32_t get_offset_of_expectedTime_17() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___expectedTime_17)); }
	inline float get_expectedTime_17() const { return ___expectedTime_17; }
	inline float* get_address_of_expectedTime_17() { return &___expectedTime_17; }
	inline void set_expectedTime_17(float value)
	{
		___expectedTime_17 = value;
	}

	inline static int32_t get_offset_of_didGetCorrectOnUpdate_18() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___didGetCorrectOnUpdate_18)); }
	inline bool get_didGetCorrectOnUpdate_18() const { return ___didGetCorrectOnUpdate_18; }
	inline bool* get_address_of_didGetCorrectOnUpdate_18() { return &___didGetCorrectOnUpdate_18; }
	inline void set_didGetCorrectOnUpdate_18(bool value)
	{
		___didGetCorrectOnUpdate_18 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__13_19() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__13_19)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__13_19() const { return ___U3CU3E9__13_19; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__13_19() { return &___U3CU3E9__13_19; }
	inline void set_U3CU3E9__13_19(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__13_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__13_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_20() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__14_20)); }
	inline Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * get_U3CU3E9__14_20() const { return ___U3CU3E9__14_20; }
	inline Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B ** get_address_of_U3CU3E9__14_20() { return &___U3CU3E9__14_20; }
	inline void set_U3CU3E9__14_20(Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * value)
	{
		___U3CU3E9__14_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__14_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_21() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__16_21)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__16_21() const { return ___U3CU3E9__16_21; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__16_21() { return &___U3CU3E9__16_21; }
	inline void set_U3CU3E9__16_21(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__16_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__16_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_22() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__15_22)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_U3CU3E9__15_22() const { return ___U3CU3E9__15_22; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_U3CU3E9__15_22() { return &___U3CU3E9__15_22; }
	inline void set_U3CU3E9__15_22(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___U3CU3E9__15_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_22), (void*)value);
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// DentedPixel.LTExamples.TestingUnitTests
struct TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube1_4;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube2_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube3
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube3_6;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube4
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube4_7;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeAlpha1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeAlpha1_8;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeAlpha2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeAlpha2_9;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::eventGameObjectWasCalled
	bool ___eventGameObjectWasCalled_10;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::eventGeneralWasCalled
	bool ___eventGeneralWasCalled_11;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::lt1Id
	int32_t ___lt1Id_12;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt2
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___lt2_13;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt3
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___lt3_14;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt4
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___lt4_15;
	// LTDescr[] DentedPixel.LTExamples.TestingUnitTests::groupTweens
	LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* ___groupTweens_16;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests::groupGOs
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___groupGOs_17;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::groupTweensCnt
	int32_t ___groupTweensCnt_18;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::rotateRepeat
	int32_t ___rotateRepeat_19;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::rotateRepeatAngle
	int32_t ___rotateRepeatAngle_20;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::boxNoCollider
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___boxNoCollider_21;
	// System.Single DentedPixel.LTExamples.TestingUnitTests::timeElapsedNormalTimeScale
	float ___timeElapsedNormalTimeScale_22;
	// System.Single DentedPixel.LTExamples.TestingUnitTests::timeElapsedIgnoreTimeScale
	float ___timeElapsedIgnoreTimeScale_23;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::pauseTweenDidFinish
	bool ___pauseTweenDidFinish_24;

public:
	inline static int32_t get_offset_of_cube1_4() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube1_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube1_4() const { return ___cube1_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube1_4() { return &___cube1_4; }
	inline void set_cube1_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube1_4), (void*)value);
	}

	inline static int32_t get_offset_of_cube2_5() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube2_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube2_5() const { return ___cube2_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube2_5() { return &___cube2_5; }
	inline void set_cube2_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube2_5), (void*)value);
	}

	inline static int32_t get_offset_of_cube3_6() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube3_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube3_6() const { return ___cube3_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube3_6() { return &___cube3_6; }
	inline void set_cube3_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube3_6), (void*)value);
	}

	inline static int32_t get_offset_of_cube4_7() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube4_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube4_7() const { return ___cube4_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube4_7() { return &___cube4_7; }
	inline void set_cube4_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube4_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube4_7), (void*)value);
	}

	inline static int32_t get_offset_of_cubeAlpha1_8() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cubeAlpha1_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeAlpha1_8() const { return ___cubeAlpha1_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeAlpha1_8() { return &___cubeAlpha1_8; }
	inline void set_cubeAlpha1_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeAlpha1_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeAlpha1_8), (void*)value);
	}

	inline static int32_t get_offset_of_cubeAlpha2_9() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cubeAlpha2_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeAlpha2_9() const { return ___cubeAlpha2_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeAlpha2_9() { return &___cubeAlpha2_9; }
	inline void set_cubeAlpha2_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeAlpha2_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeAlpha2_9), (void*)value);
	}

	inline static int32_t get_offset_of_eventGameObjectWasCalled_10() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___eventGameObjectWasCalled_10)); }
	inline bool get_eventGameObjectWasCalled_10() const { return ___eventGameObjectWasCalled_10; }
	inline bool* get_address_of_eventGameObjectWasCalled_10() { return &___eventGameObjectWasCalled_10; }
	inline void set_eventGameObjectWasCalled_10(bool value)
	{
		___eventGameObjectWasCalled_10 = value;
	}

	inline static int32_t get_offset_of_eventGeneralWasCalled_11() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___eventGeneralWasCalled_11)); }
	inline bool get_eventGeneralWasCalled_11() const { return ___eventGeneralWasCalled_11; }
	inline bool* get_address_of_eventGeneralWasCalled_11() { return &___eventGeneralWasCalled_11; }
	inline void set_eventGeneralWasCalled_11(bool value)
	{
		___eventGeneralWasCalled_11 = value;
	}

	inline static int32_t get_offset_of_lt1Id_12() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt1Id_12)); }
	inline int32_t get_lt1Id_12() const { return ___lt1Id_12; }
	inline int32_t* get_address_of_lt1Id_12() { return &___lt1Id_12; }
	inline void set_lt1Id_12(int32_t value)
	{
		___lt1Id_12 = value;
	}

	inline static int32_t get_offset_of_lt2_13() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt2_13)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_lt2_13() const { return ___lt2_13; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_lt2_13() { return &___lt2_13; }
	inline void set_lt2_13(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___lt2_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt2_13), (void*)value);
	}

	inline static int32_t get_offset_of_lt3_14() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt3_14)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_lt3_14() const { return ___lt3_14; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_lt3_14() { return &___lt3_14; }
	inline void set_lt3_14(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___lt3_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt3_14), (void*)value);
	}

	inline static int32_t get_offset_of_lt4_15() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt4_15)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_lt4_15() const { return ___lt4_15; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_lt4_15() { return &___lt4_15; }
	inline void set_lt4_15(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___lt4_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt4_15), (void*)value);
	}

	inline static int32_t get_offset_of_groupTweens_16() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___groupTweens_16)); }
	inline LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* get_groupTweens_16() const { return ___groupTweens_16; }
	inline LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F** get_address_of_groupTweens_16() { return &___groupTweens_16; }
	inline void set_groupTweens_16(LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* value)
	{
		___groupTweens_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groupTweens_16), (void*)value);
	}

	inline static int32_t get_offset_of_groupGOs_17() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___groupGOs_17)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_groupGOs_17() const { return ___groupGOs_17; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_groupGOs_17() { return &___groupGOs_17; }
	inline void set_groupGOs_17(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___groupGOs_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groupGOs_17), (void*)value);
	}

	inline static int32_t get_offset_of_groupTweensCnt_18() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___groupTweensCnt_18)); }
	inline int32_t get_groupTweensCnt_18() const { return ___groupTweensCnt_18; }
	inline int32_t* get_address_of_groupTweensCnt_18() { return &___groupTweensCnt_18; }
	inline void set_groupTweensCnt_18(int32_t value)
	{
		___groupTweensCnt_18 = value;
	}

	inline static int32_t get_offset_of_rotateRepeat_19() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___rotateRepeat_19)); }
	inline int32_t get_rotateRepeat_19() const { return ___rotateRepeat_19; }
	inline int32_t* get_address_of_rotateRepeat_19() { return &___rotateRepeat_19; }
	inline void set_rotateRepeat_19(int32_t value)
	{
		___rotateRepeat_19 = value;
	}

	inline static int32_t get_offset_of_rotateRepeatAngle_20() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___rotateRepeatAngle_20)); }
	inline int32_t get_rotateRepeatAngle_20() const { return ___rotateRepeatAngle_20; }
	inline int32_t* get_address_of_rotateRepeatAngle_20() { return &___rotateRepeatAngle_20; }
	inline void set_rotateRepeatAngle_20(int32_t value)
	{
		___rotateRepeatAngle_20 = value;
	}

	inline static int32_t get_offset_of_boxNoCollider_21() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___boxNoCollider_21)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_boxNoCollider_21() const { return ___boxNoCollider_21; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_boxNoCollider_21() { return &___boxNoCollider_21; }
	inline void set_boxNoCollider_21(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___boxNoCollider_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boxNoCollider_21), (void*)value);
	}

	inline static int32_t get_offset_of_timeElapsedNormalTimeScale_22() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___timeElapsedNormalTimeScale_22)); }
	inline float get_timeElapsedNormalTimeScale_22() const { return ___timeElapsedNormalTimeScale_22; }
	inline float* get_address_of_timeElapsedNormalTimeScale_22() { return &___timeElapsedNormalTimeScale_22; }
	inline void set_timeElapsedNormalTimeScale_22(float value)
	{
		___timeElapsedNormalTimeScale_22 = value;
	}

	inline static int32_t get_offset_of_timeElapsedIgnoreTimeScale_23() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___timeElapsedIgnoreTimeScale_23)); }
	inline float get_timeElapsedIgnoreTimeScale_23() const { return ___timeElapsedIgnoreTimeScale_23; }
	inline float* get_address_of_timeElapsedIgnoreTimeScale_23() { return &___timeElapsedIgnoreTimeScale_23; }
	inline void set_timeElapsedIgnoreTimeScale_23(float value)
	{
		___timeElapsedIgnoreTimeScale_23 = value;
	}

	inline static int32_t get_offset_of_pauseTweenDidFinish_24() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___pauseTweenDidFinish_24)); }
	inline bool get_pauseTweenDidFinish_24() const { return ___pauseTweenDidFinish_24; }
	inline bool* get_address_of_pauseTweenDidFinish_24() { return &___pauseTweenDidFinish_24; }
	inline void set_pauseTweenDidFinish_24(bool value)
	{
		___pauseTweenDidFinish_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3245[23] = 
{
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_pauseCount_1(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeRound_2(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_onStartPos_3(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_onStartPosSpline_4(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeSpline_5(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeSeq_6(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeBounds_7(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_didPassBounds_8(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_failPoint_9(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_setOnStartNum_10(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_setPosOnUpdate_11(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_setPosNum_12(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_hasGroupTweensCheckStarted_13(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_previousXlt4_14(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_onUpdateWasCalled_15(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_start_16(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_expectedTime_17(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_didGetCorrectOnUpdate_18(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__13_19(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__14_20(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__16_21(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__15_22(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3246[5] = 
{
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E1__state_0(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E2__current_1(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E4__this_2(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E8__1_3(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CdescriptionMatchCountU3E5__2_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3247[8] = 
{
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CU3E1__state_0(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CU3E2__current_1(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CU3E4__this_2(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CcubeCountU3E5__2_3(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CtweensAU3E5__3_4(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CaGOsU3E5__4_5(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CtweensBU3E5__5_6(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CbGOsU3E5__6_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3248[3] = 
{
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB::get_offset_of_U3CU3E1__state_0(),
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB::get_offset_of_U3CU3E2__current_1(),
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3249[21] = 
{
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube1_4(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube2_5(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube3_6(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube4_7(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cubeAlpha1_8(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cubeAlpha2_9(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_eventGameObjectWasCalled_10(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_eventGeneralWasCalled_11(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt1Id_12(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt2_13(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt3_14(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt4_15(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_groupTweens_16(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_groupGOs_17(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_groupTweensCnt_18(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_rotateRepeat_19(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_rotateRepeatAngle_20(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_boxNoCollider_21(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_timeElapsedNormalTimeScale_22(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_timeElapsedIgnoreTimeScale_23(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_pauseTweenDidFinish_24(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void SimpleShare::Awake()
extern void SimpleShare_Awake_m3FE222BEB9913F27824B307E498BC8C3CA58CC4B (void);
// 0x00000002 System.Void SimpleShare::Loaded()
extern void SimpleShare_Loaded_m7E746682F1A77676A333C16FCD2559B562128EE9 (void);
// 0x00000003 System.Void SimpleShare::AndroidShare(System.String)
extern void SimpleShare_AndroidShare_m2723B393EA578AF89B397A447A06AFD3E9B3662E (void);
// 0x00000004 System.Void SimpleShare::ShareAndroidScreenShot(System.String,System.String,System.String)
extern void SimpleShare_ShareAndroidScreenShot_mADFD20EA04006B86D69584DAB6FB255E58977D26 (void);
// 0x00000005 System.Void SimpleShare::ShareAndroidRect(UnityEngine.Rect,System.String,System.String,System.String)
extern void SimpleShare_ShareAndroidRect_mE4ED2DFE260D8B2A2D09963C3827BBBFF1399C56 (void);
// 0x00000006 System.Void SimpleShare::ShareAndroidRectTransform(UnityEngine.RectTransform,System.String,System.String,System.String)
extern void SimpleShare_ShareAndroidRectTransform_m343A10909F94DA2D40F76E3B4D8F410F323A8013 (void);
// 0x00000007 System.Void SimpleShare::ShareAndroid(System.String,System.String,System.String,System.String)
extern void SimpleShare_ShareAndroid_m154D28E77270ADD900AFF80985C871D21D85E150 (void);
// 0x00000008 System.Boolean SimpleShare::AndroidFileExists(System.String)
extern void SimpleShare_AndroidFileExists_m11F6413A4711766A9718A1AA4DB153967DE99AF0 (void);
// 0x00000009 System.Void SimpleShare::ShareText(System.String,System.String,System.Single,System.Single)
extern void SimpleShare_ShareText_m65E16A909FDC4C875AB74EAAA7AE96DADF7F4E8C (void);
// 0x0000000A System.Void SimpleShare::ShareScreenshot(System.String,System.String,System.Single,System.Single)
extern void SimpleShare_ShareScreenshot_mAAE3A3870E30B3820E9A8A4348F39602029A8062 (void);
// 0x0000000B System.Void SimpleShare::ShareRect(UnityEngine.Rect,System.String,System.String,System.Single,System.Single)
extern void SimpleShare_ShareRect_mCCAB8F799F186F6900C7342472A2D6E16C58E9C8 (void);
// 0x0000000C System.Void SimpleShare::ShareRectTransform(UnityEngine.RectTransform,System.String,System.String,System.Single,System.Single)
extern void SimpleShare_ShareRectTransform_mAB8EEDE26AAC6BE46299F7E250E43D6203AC10E1 (void);
// 0x0000000D System.Collections.IEnumerator SimpleShare::Screenshot(System.Action`2<UnityEngine.Texture2D,System.String>)
extern void SimpleShare_Screenshot_m1EE924B63456E9991D56C40F25B6ADCCF3A57934 (void);
// 0x0000000E System.Collections.IEnumerator SimpleShare::CaptureRect(UnityEngine.Rect,System.Action`2<UnityEngine.Texture2D,System.String>)
extern void SimpleShare_CaptureRect_m8822889E9375B86DBC2CC88BE8D577B07E1A931C (void);
// 0x0000000F System.String SimpleShare::SaveTexture(UnityEngine.Texture2D)
extern void SimpleShare_SaveTexture_m11E75339F20CDF54155DE1D3FCEFB381C137F937 (void);
// 0x00000010 System.String SimpleShare::FilePath(System.String)
extern void SimpleShare_FilePath_m717F389124B3E230DABC7FF9E3943DD95E16E032 (void);
// 0x00000011 UnityEngine.Rect SimpleShare::RectTransformToRect(UnityEngine.RectTransform)
extern void SimpleShare_RectTransformToRect_m7F64F0548602D8D2F2E85E7702174FAECF8A2CE6 (void);
// 0x00000012 System.Void SimpleShare::.ctor()
extern void SimpleShare__ctor_m2B9655CCD02A9152620EA56D646C0F31D59E8A13 (void);
// 0x00000013 System.Void SimpleShare::.cctor()
extern void SimpleShare__cctor_m0A677E07008CB03E7D067CC528455298E8BCC3A1 (void);
// 0x00000014 System.Void SimpleShare/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB7E8C391F74FBF19A423F44E2942CA69E9E1D22C (void);
// 0x00000015 System.Void SimpleShare/<>c__DisplayClass3_0::<AndroidShare>b__0(UnityEngine.Texture2D,System.String)
extern void U3CU3Ec__DisplayClass3_0_U3CAndroidShareU3Eb__0_mD0ABD83B8611511CAD071ECEA52784BA03649BBA (void);
// 0x00000016 System.Void SimpleShare/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mF2A56DAF2E505FA59D728A7065849C1A4EC57EA8 (void);
// 0x00000017 System.Void SimpleShare/<>c__DisplayClass4_0::<ShareAndroidScreenShot>b__0(UnityEngine.Texture2D,System.String)
extern void U3CU3Ec__DisplayClass4_0_U3CShareAndroidScreenShotU3Eb__0_m2B0E1E3F3A1E8FB24EC30A4486107D11BA8D4A5A (void);
// 0x00000018 System.Void SimpleShare/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m1EC5CF846A3FBF2D7B70920EAFFD92D13F037767 (void);
// 0x00000019 System.Void SimpleShare/<>c__DisplayClass5_0::<ShareAndroidRect>b__0(UnityEngine.Texture2D,System.String)
extern void U3CU3Ec__DisplayClass5_0_U3CShareAndroidRectU3Eb__0_m09C4AD653EE2B1C95C54E18752C8C22B247DA653 (void);
// 0x0000001A System.Void SimpleShare/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m1DA07F33FD1FC22F8B0FEB9C3E14A4EA7038A20C (void);
// 0x0000001B System.Void SimpleShare/<>c__DisplayClass6_0::<ShareAndroidRectTransform>b__0(UnityEngine.Texture2D,System.String)
extern void U3CU3Ec__DisplayClass6_0_U3CShareAndroidRectTransformU3Eb__0_m983EC6517084DC473913B0B6893899382B48843D (void);
// 0x0000001C System.Void SimpleShare/<CaptureRect>d__14::.ctor(System.Int32)
extern void U3CCaptureRectU3Ed__14__ctor_m7C884F58380E3D086B8B19C9E32606157FB96D59 (void);
// 0x0000001D System.Void SimpleShare/<CaptureRect>d__14::System.IDisposable.Dispose()
extern void U3CCaptureRectU3Ed__14_System_IDisposable_Dispose_m55C3251F0E06CB9B637F3CAFE61E3A26A7CCCFED (void);
// 0x0000001E System.Boolean SimpleShare/<CaptureRect>d__14::MoveNext()
extern void U3CCaptureRectU3Ed__14_MoveNext_mBFA0BC7F3851CF86AFD1CB04C23546B29FD73E9E (void);
// 0x0000001F System.Object SimpleShare/<CaptureRect>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCaptureRectU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C74BC874FE2E65C9672480124FD4BEC1A2A27AC (void);
// 0x00000020 System.Void SimpleShare/<CaptureRect>d__14::System.Collections.IEnumerator.Reset()
extern void U3CCaptureRectU3Ed__14_System_Collections_IEnumerator_Reset_mF050FE1CC2C6A64BF1A74941124629F3D2529A1F (void);
// 0x00000021 System.Object SimpleShare/<CaptureRect>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CCaptureRectU3Ed__14_System_Collections_IEnumerator_get_Current_mC952930934A371087D647E3E7CC47A64E561A058 (void);
static Il2CppMethodPointer s_methodPointers[33] = 
{
	SimpleShare_Awake_m3FE222BEB9913F27824B307E498BC8C3CA58CC4B,
	SimpleShare_Loaded_m7E746682F1A77676A333C16FCD2559B562128EE9,
	SimpleShare_AndroidShare_m2723B393EA578AF89B397A447A06AFD3E9B3662E,
	SimpleShare_ShareAndroidScreenShot_mADFD20EA04006B86D69584DAB6FB255E58977D26,
	SimpleShare_ShareAndroidRect_mE4ED2DFE260D8B2A2D09963C3827BBBFF1399C56,
	SimpleShare_ShareAndroidRectTransform_m343A10909F94DA2D40F76E3B4D8F410F323A8013,
	SimpleShare_ShareAndroid_m154D28E77270ADD900AFF80985C871D21D85E150,
	SimpleShare_AndroidFileExists_m11F6413A4711766A9718A1AA4DB153967DE99AF0,
	SimpleShare_ShareText_m65E16A909FDC4C875AB74EAAA7AE96DADF7F4E8C,
	SimpleShare_ShareScreenshot_mAAE3A3870E30B3820E9A8A4348F39602029A8062,
	SimpleShare_ShareRect_mCCAB8F799F186F6900C7342472A2D6E16C58E9C8,
	SimpleShare_ShareRectTransform_mAB8EEDE26AAC6BE46299F7E250E43D6203AC10E1,
	SimpleShare_Screenshot_m1EE924B63456E9991D56C40F25B6ADCCF3A57934,
	SimpleShare_CaptureRect_m8822889E9375B86DBC2CC88BE8D577B07E1A931C,
	SimpleShare_SaveTexture_m11E75339F20CDF54155DE1D3FCEFB381C137F937,
	SimpleShare_FilePath_m717F389124B3E230DABC7FF9E3943DD95E16E032,
	SimpleShare_RectTransformToRect_m7F64F0548602D8D2F2E85E7702174FAECF8A2CE6,
	SimpleShare__ctor_m2B9655CCD02A9152620EA56D646C0F31D59E8A13,
	SimpleShare__cctor_m0A677E07008CB03E7D067CC528455298E8BCC3A1,
	U3CU3Ec__DisplayClass3_0__ctor_mB7E8C391F74FBF19A423F44E2942CA69E9E1D22C,
	U3CU3Ec__DisplayClass3_0_U3CAndroidShareU3Eb__0_mD0ABD83B8611511CAD071ECEA52784BA03649BBA,
	U3CU3Ec__DisplayClass4_0__ctor_mF2A56DAF2E505FA59D728A7065849C1A4EC57EA8,
	U3CU3Ec__DisplayClass4_0_U3CShareAndroidScreenShotU3Eb__0_m2B0E1E3F3A1E8FB24EC30A4486107D11BA8D4A5A,
	U3CU3Ec__DisplayClass5_0__ctor_m1EC5CF846A3FBF2D7B70920EAFFD92D13F037767,
	U3CU3Ec__DisplayClass5_0_U3CShareAndroidRectU3Eb__0_m09C4AD653EE2B1C95C54E18752C8C22B247DA653,
	U3CU3Ec__DisplayClass6_0__ctor_m1DA07F33FD1FC22F8B0FEB9C3E14A4EA7038A20C,
	U3CU3Ec__DisplayClass6_0_U3CShareAndroidRectTransformU3Eb__0_m983EC6517084DC473913B0B6893899382B48843D,
	U3CCaptureRectU3Ed__14__ctor_m7C884F58380E3D086B8B19C9E32606157FB96D59,
	U3CCaptureRectU3Ed__14_System_IDisposable_Dispose_m55C3251F0E06CB9B637F3CAFE61E3A26A7CCCFED,
	U3CCaptureRectU3Ed__14_MoveNext_mBFA0BC7F3851CF86AFD1CB04C23546B29FD73E9E,
	U3CCaptureRectU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C74BC874FE2E65C9672480124FD4BEC1A2A27AC,
	U3CCaptureRectU3Ed__14_System_Collections_IEnumerator_Reset_mF050FE1CC2C6A64BF1A74941124629F3D2529A1F,
	U3CCaptureRectU3Ed__14_System_Collections_IEnumerator_get_Current_mC952930934A371087D647E3E7CC47A64E561A058,
};
static const int32_t s_InvokerIndices[33] = 
{
	1749,
	1749,
	1451,
	543,
	365,
	350,
	350,
	1257,
	355,
	355,
	184,
	177,
	1131,
	643,
	1131,
	1131,
	1150,
	1749,
	2840,
	1749,
	872,
	1749,
	872,
	1749,
	872,
	1749,
	872,
	1441,
	1749,
	1726,
	1706,
	1749,
	1706,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	33,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
